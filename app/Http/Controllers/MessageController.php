<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Message;

use App\Repositories\MessageRepository;

class MessageController extends Controller
{
    //
    public function __construct(MessageRepository $messages)
    {
        $this->middleware('auth');
        $this->messages = $messages;
    }

    
    /**
     * Create a new message.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:30',
            'content' => 'required|max:1000',
            'category' => 'required|max:10',
        ]);
        $path = $request->file('image')->store('message-images', 'public');
        $request->user()->messages()->create([
            'title' => $request->title,
            'content' => $request->content,
            'category' => $request->category,
            'image' => $path,
        ]);
        return redirect('/messages');
    }

    /**
     * Display a list of all users' messages.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $messages = Message::all();
        //$messages = $request->user()->messages()->get();
        return view('messages.index', [
            'messages' => $messages,
        ]);
    }

    /**
     * Destroy the given message.
     *
     * @param  Request  $request
     * @param  Message  $message
     * @return Response
     */
    public function destroy(Request $request, Message $message)
    {
        $this->authorize('destroy', $message);
        $message->delete();
        return redirect('/messages');
    }


    public function show(Message $id)//$id)
    {
        $message = Message::find($id);
        return $id;
    }
}
