<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('welcome');    
});


Route::auth();
Route::get('/logout', 'Auth\LoginController@logout');


Route::get('/messages', 'MessageController@index');
Route::post('/message', 'MessageController@store');
Route::post('/message_delete/{message}', 'MessageController@destroy');
Route::get('/messages/{message}', 'MessageController@show');

Route::get('/post_message', function () {
    return view('post_message.index');
});