<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = ['title','content','category','image'];

    // Get which user wrote this message
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
