@extends('layouts.app')
@section('content')
    <!-- Bootstrap 樣板... -->
    <div class="panel-body">
        <!-- 顯示驗證錯誤 -->
        @include('common.errors')
    </div>
    <!-- 顯示目前留言 -->
    {{-- @if (0 > 0)
        <div class="panel panel-default, center ">
            <div class="panel-heading">
                Current Messages
            </div>
            <div class="panel-body">
                <table class="table table-striped message-table">
                    <!-- Table Headings -->
                    <thead>
                        <th>Content</th>
                        <th>&nbsp;</th>
                    </thead>
                    <!-- Table Body -->
                    <tbody>
                        @foreach ($messages as $message)
                            <tr>
                                <!-- Message Name -->
                                <td class="table-text">
					            <a href="{{ url('messages/'.$message->id) }}"><div>{{ $message->title }}</div></a>
                                </td>
                                <!-- Delete Button -->
                                <td>
                                    @if (Auth::user()->id==$message->user_id)
                                    <form action="{{ url('message/'.$message->id) }}" method="POST">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}
                                        <button type="submit" id="delete-message-{{ $message->id }}" class="btn btn-danger">
                                            <i class="fa fa-btn fa-trash"></i>Delete
                                        </button>
                                    </form>
                                    @endif
                                </td>
                                <!-- Edit Button -->
                                <td>
                                    @if (Auth::user()->id==$message->user_id)
                                        <form action="{{ url('messages/'.$message->id.'/edit') }}" method="GET">
                                            <button type="submit" id="edit-message-{{ $message->id }}" class="btn btn-info">
                                                <i class="fa fa-pencil fa-fw"></i>Edit
                                            </button>
                                        </form>
                                        @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif --}}
    @if (count($messages) > 0)
        <div class="panel panel-default, center ">
            <div class="panel-heading">
                公告
            </div>
            <div class="panel-body">
                <div class="row">
                    @foreach ($messages as $message)
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <img src="{{ asset('storage/' . $message->image)}} " alt="message image">
                                <div class="caption">
                                    <h3>{{ $message->title }}</h3>
                                    <p>{{ $message->category }}</p>
                                    <p><a href="{{ url('messages/'.$message->id) }}" class="btn btn-primary" role="button">查看更多</a>
                                    @if (Auth::user()->id==$message->user_id)
                                        <form action="{{ url('message_delete/'.$message->id) }}" method="POST">
                                            {!! csrf_field() !!}
                                            {{-- {!! method_field('DELETE') !!} --}}
                                            <button type="submit" id="delete-message-{{ $message->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>刪除
                                            </button>
                                        </form>
                                    @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    
@endsection
