@extends('layouts.app')
@section('content')
    <!-- Bootstrap 樣板... -->
    <div class="panel-body">
        <!-- 顯示驗證錯誤 -->
        @include('common.errors')
        <!-- 新留言的表單 -->
        <form action="{{ url('message') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
            {{ csrf_field() }}
            <!-- 留言內容 -->
            <div class="form-group">
                <label for="message-title" class="col-sm-3 control-label">標題: </label>
                <div class="col-sm-6">
                    <input type="text" name="title" id="message-title" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="message-content" class="col-sm-3 control-label">內容: </label>
                <div class="col-sm-6">
                    <textarea type="text" name="content" id="message-content" class="form-control" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="message-category" class="col-sm-3 control-label">分類: </label>
                <div class="col-sm-6">
                    <select class="form-control" name="category" id="message-category">
                        <option selected>請選擇...</option>
                        <option value="重要公告">重要公告</option>
                        <option value="公告">公告</option>
                        <option value="活動">活動</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="message-image" class="col-sm-3 control-label">圖片: </label>
                <div class="col-sm-6">
                    <input type="file" name="image" id="message-image">
                </div>
            </div>
            <!-- 增加留言按鈕-->
            <div class="form-group" style='text-align:center'>
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> 公告
                    </button>
                </div>
            </div>
        </form>
    </div>
    
@endsection